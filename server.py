#!/usr/bin/env python

import socket
import sys

HOST = '127.0.0.1'
PORT = 50010

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    sock.connect(HOST, PORT)
except Exception, err:
    print 'ERROR | unable to connect'
    sys.exit(1)
else:
    sock.sendall('I am here')