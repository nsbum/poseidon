#!/usr/bin/env python

from flask import Flask,session
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, AnonymousUser,
                            confirm_login, fresh_login_required)

#   setup our app and read the config
app = Flask(__name__)
app.config.from_object('poseidon.config')

#   setup our db from the app
db = SQLAlchemy(app)

from poseidon import views
from poseidon.helper.status import StatusCode