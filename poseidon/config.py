import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir,'poseidon.db')

SECRET_KEY = 'E28CE6AC-2292-4846-A6AA-87274DEE723438AFCC08-5D68-420E-B3DA-A8D0B505670E'
DEBUG = True