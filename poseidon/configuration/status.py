#!/usr/bin/env python

"""
    StatusCode class is a Python equivalent of an enum
    There are several ways to implements "enum-like" functionality in Python; see:
    http://stackoverflow.com/questions/36932/whats-the-best-way-to-implement-an-enum-in-python;
    but this is the one we picked for this app.
"""
class StatusCode():

    (SUCCESS,SUCCESS_NO_ROWS,FAIL_CONNECT,FAIL_QUERY,FAIL_MISSING_PARAM,FAIL_OTHER,FAIL_EXPIRED_SESSION_ID) = range(7);