#!/usr/bin/env python

from socket import *
import thread
import threading

# server buffer size
BUFF = 1024
# server host and port
HOST = '127.0.0.1'
PORT = 50010

def response(key):
    return 'Server response: ' + key

def handler(clientsock,addr):
    while 1:
        data = clientsock.recv(BUFF)
        if not data: break
        print repr(addr) + ' recv:' + repr(data)
        clientsock.send(response(data))
        print repr(addr) + ' sent:' + repr(response(data))
        if "close" == data.rstrip(): break # type 'close' on client console to close connection from the server side

    clientsock.close()
    print addr, "- closed connection" #log on console

class PoolControllerThread(threading.Thread):
    def __init__(self, ):
        threading.Thread.__init__(self)
    
    def run(self, ):
        pass
    
if __name__ == "__main__":
    #   start the system control thread
    pool_controller = PoolControllerThread()
    pool_controller.start()
    
    #   start the server
    ADDR = (HOST, PORT)
    serversock = socket(AF_INET, SOCK_STREAM)
    serversock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    serversock.bind(ADDR)
    serversock.listen(5)
    while 1:
        print 'waiting for connection... listening on port', PORT
        clientsock, addr = serversock.accept()
        print '...connected from:', addr
        thread.start_new_thread(handler, (clientsock, addr))
    
    print "Terminated"
    
